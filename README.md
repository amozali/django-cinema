# پروژه‌ی آموزشی فروش بلیط سینما برآمده از خودآموز [مکتب‌خونه][-1]

## راهنما

تمام وابستگی‌های این پروژه توسط داکر مجزاسازی شده؛ پس باید بر روی سیستم عاملتان نصب شده باشد. همچنین از داکر کامپوز که ابزار ساده‌سازی کار با موتور داکر است هم استفاده شده است. روش نصب هردواِشان بر روی اوبونتو را در ادامه آورده‌ام.

## [نصب داکر (Docker) روی اوبونتو][0]

از این ابزار برای مجزا سازی (isolation) استفاده می‌شود. تقریبا همانند ماشین مجازی است. تمام وابستگی‌های اپ در یک امیج نصب شده است. می‌توانید با ویرایش فایل‌های Dotfiles (وابسته به سیستم‌عامل) یا requirements.txt (وابسته به پایتون) وابستگی‌ها را مدیریت کنید.

### [نصب مخزن][2]

برای نصب موتور داکر نیازمند به افزودن مخزنش هستیم.

```bash
# 1- Update the apt package index:
sudo apt-get update

# 2- Install packages to allow apt to use a repository over HTTPS:
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

# 3- Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# 4- Use the following command to set up the stable repository.
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

### [نصب موتور داکر][2]

```bash
# 1- Update the apt package index.
sudo apt-get update

# 2- Install the latest version of Docker Engine - Community and containerd:
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

### [مدیریت داکر از کاربر-نامدیر][3]

اگر می‌خواهید مجبور نباشید داکر را با کاربر ریشه (root) اجرا کنید دستورهای زیر را اجرا کنید.

```bash
# 1- Create the docker group.
sudo groupadd docker

# 2- Add your user to the docker group.
sudo usermod -aG docker $USER

# 3- Log out and log back in so that your group membership is re-evaluated. On Linux, you can also run the following command to activate the changes to groups:
newgrp docker

# 4- Verify that you can run docker commands without sudo.
# This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.
docker run hello-world
```

### [داکر کامپوز (Docker Compose)][4] 

از این ابزار برای ایجاد ایمج داکر استفاده می‌کنیم. یعنی، تمام فرمان‌ها را در فایل `*.yml` یا `*.yaml` می‌نوسیم تا با داکر برایمان ایمج (چیزی مانند ماشین مجازی) ایجاد کند.

```bash
# 1- Run this command to download the current stable release of Docker Compose:
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# 2- Apply executable permissions to the binary:
sudo chmod +x /usr/local/bin/docker-compose
```
## اجرا
برای بالا آوردن اپ از دستور زیر در مسیر پروژه استفاده کنید. این دستور تمام نیازمندی‌های اپ را بارگیری و ایمیج‌ و کانتینراش را ایجاد می‌کند.

```bash
docker-compose up
# Or
docker-compose up -d
```

 اپ از مسیر `http://localhost:8084` دردسترس است. می‌توانید پورت ۸۰۸۴ را در فایل `docker-compose.yml` تغییر دهید.

## دستورهای پرکاربرد داکر کامپوز

```bash
# شروعاندن کانتینر
docker-compose start

# ایستاندن کانتینر
docker-compose stop

# بازآغازاندن کانتینر
docker-compose restart

# حذفاندن کانتینر
docker-compose down
```

[-1]: https://maktabkhooneh.org/course/%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%AC%D9%86%DA%AF%D9%88-django-mk623/
[0]: https://www.docker.com/
[1]: https://docs.docker.com/
[2]: https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository
[3]: https://docs.docker.com/install/linux/linux-postinstall/
[4]: https://docs.docker.com/compose/install/
