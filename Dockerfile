ARG PYTHON=3.8

FROM python:${PYTHON}-alpine

ARG USERNAME=admin
ARG WORKDIR=/home/${USERNAME}/project/

ENV PYTHONUNBUFFERED 1

WORKDIR ${WORKDIR}

COPY source/ requirements.txt .flake8 ./

RUN adduser -D ${USERNAME}; \
    # Requirements for installing Pillow
    apk add --no-cache build-base jpeg-dev zlib-dev; \
    pip install -r requirements.txt;

USER ${USERNAME}
